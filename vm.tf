

resource "vscale_scalet" "web" {
   
  make_from = "ubuntu_14.04_64_002_master"
  rplan = "small"
  name = "New-Test"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.web.id}"]
}
